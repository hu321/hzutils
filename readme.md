# hzutils introduction

## path

* 删除过期文件

```python
from hzutils.path import clear_old_media_file
clear_old_media_file(dirname, days)
```

## spider

* 浏览器 cookie 字符串转字典
* 浏览器 header 字符串转字典

## urllib

* start_url 从 模板 生成 的函数
