from hzutils.spider import cookies_to_dict, headers_to_dict


def test_cookies_to_dict():
    cookies_str = """
    ig_did=2987234234; mid=YFFaewALAAGU; ig_nrcb=1; shbid=3306; rur=FRC; csrftoken=uNSZK6mQ1IEa8kbFsUuFZIYeeeepfeJN; ds_user_id=5555555; sessionid=abcdefg; shbts=1616550979.9095342
    """
    cookies = cookies_to_dict(cookies_str)
    assert cookies.get("ig_did") == "2987234234"
    assert cookies.get("mid") == "YFFaewALAAGU"
    assert cookies.get("shbts") == "1616550979.9095342"

def test_headers_to_dict():
    headers_str = """:authority: www.instagram.com
    :method: GET
    :path: /explore/tags/%E5%8F%B8%E9%A6%AC%E6%96%87/?__a=1
    :scheme: https
    accept: */*
    accept-encoding: gzip, deflate, br
    accept-language: zh-CN,zh;q=0.9,en;q=0.8,ru;q=0.7
    cookie: ig_did=2987234234; mid=YFFaewALAAGU; ig_nrcb=1; shbid=3306; rur=FRC; csrftoken=uNSZK6mQ1IEa8kbFsUuFZIYeeeepfeJN; ds_user_id=5555555; sessionid=abcdefg; shbts=1616550979.9095342
    referer: https://www.instagram.com/explore/tags/%E5%8F%B8%E9%A6%AC%E6%96%87/
    sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"
    sec-ch-ua-mobile: ?0
    sec-fetch-dest: empty
    sec-fetch-mode: cors
    sec-fetch-site: same-origin
    user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.104 Safari/537.36
    x-ig-app-id: 936619743392459
    x-ig-www-claim: hmac.AR0epI8htYnByz-mtvftnjBYKnujzQ95-Ug_JeKQmwYznz6v
    x-requested-with: XMLHttpRequest"""
    headers = headers_to_dict(headers_str)
    assert headers.get("accept") == "*/*"
    assert ":method" not in headers
    print(headers)