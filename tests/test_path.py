from hzutils.path import clear_old_media_file
import pytest
import pathlib
import shutil
import os

# @pytest.fixture(scope='function')
# def setup_function(request):
#     def teardown_function():
#         pass
#     request.addfinalizer(teardown_function)

@pytest.fixture(scope='function')
def create_media_files(request):
    pathlib.Path("tests/data").mkdir(exist_ok=True)
    pathlib.Path("tests/data/a.jpg").touch()
    assert len(os.listdir("tests/data")) == 1
    def logout():
        # pathlib.Path("tests/data").unlink()
        shutil.rmtree("tests/data")
        print("目录清理完毕")
    request.addfinalizer(logout)

def test_path(create_media_files):
    clear_old_media_file("tests/data", -1)
    assert len(os.listdir("tests/data")) == 0
    print("done!666")
    