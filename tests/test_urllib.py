from hzutils.urllib import get_start_urls


def test_start_url():
    urls = ["https://www.baidu.com/p/[1-5]"]
    start_urls = get_start_urls(urls)
    assert len(start_urls) == 5
    assert "https://www.baidu.com/p/1" in start_urls
    assert "https://www.baidu.com/p/2" in start_urls
    assert "https://www.baidu.com/p/3" in start_urls
    assert "https://www.baidu.com/p/4" in start_urls
    assert "https://www.baidu.com/p/5" in start_urls
    
def test_start_url_2():
    urls = ["https://www.baidu.com/p/[001-100:10]"]
    start_urls = get_start_urls(urls)
    assert len(start_urls) == 10
    assert "https://www.baidu.com/p/011" in start_urls
    assert "https://www.baidu.com/p/010" not in start_urls