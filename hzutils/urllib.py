import re


def get_start_urls(start_url):
    if not isinstance(start_url, list):
        e = [start_url]
    else:
        e = start_url
    start_urls = []
    for _e in e:
        n = re.match(r"(^.*?)\[(\d+)\-(\d+)(:(\d+))?\](.*)$", _e)
        if n:
            n = n.groups()
            start = int(n[1], 10)
            end = int(n[2], 10)
            step = 1
            if n[4] is not None:
                step = int(n[4], 10)
            for i in range(start, end+1, step):
                if len(n[1]) == len(n[2]):
                    start_urls.append(n[0]+pad_zero(str(i), len(n[1]))+n[5])
                else:
                    start_urls.append(n[0]+str(i)+n[5])
        else:
            start_urls.append(_e)
    return start_urls


def pad_zero(e, t):
    while(len(e) < t):
        e = "0" + e
    return e
