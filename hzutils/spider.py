import re


def cookies_to_dict(cookies):
    """
    Convert Chrome/Firefox browser cookie string into python dict
    """
    return dict(re.findall("([^=\s]*)=(.*?)(?:;|$|\n)", cookies))


def headers_to_dict(headers):
    """
    Convert Chrome/Firefox headers string into python dict
    """
    headers_dict = dict(re.findall(r"(\S+):\s*(\S.*)", headers))
    headers_dict.pop(":authority", None)
    headers_dict.pop(":method", None)
    headers_dict.pop(":path", None)
    headers_dict.pop(":scheme", None)
    return headers_dict