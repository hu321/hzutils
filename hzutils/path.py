import pathlib
import datetime
import itertools


def clear_old_media_file(dirname, days, globs=[r"*.mp4", r"*.jpg"]):
    """删除指定目录下的mp4，jpg后缀且创建超过指定天数的文件
    """
    begin_date = datetime.date.today() - datetime.timedelta(days=days)
    for f in itertools.chain(
            *(pathlib.Path(dirname).glob(_glob)
            for _glob in globs
            )):
        ctime = datetime.datetime.date(
            datetime.datetime.fromtimestamp(f.stat().st_ctime))
        if ctime < begin_date:
            f.unlink()